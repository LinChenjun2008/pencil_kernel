# 目录
* [平台](#平台)
* [依赖](#依赖)
* [编译](#编译)
* [安装](#安装)
* [在qemu中运行](#在qemu中运行)
* [参考资料](#参考资料)
# 平台

你应该能在以下平台成功编译Pencil-Kernel:
* Windows 7 或更高版本
* Linux
# 依赖
要编译Pencil-Kernel,你需要:
* [Mingw64](https://www.mingw-w64.org/)
* [x86_64-elf-tools](https://github.com/lordmilko/i686-elf-tools/releases/tag/7.1.0)
* make

要运行Pencil-Kernel,你需要:
* [qemu-system-x86_64](https://www.qemu.org)
* Ovmf


在Linux上,你可以使用下方的命令安装所需程序:
```sh
sudo apt-get install gcc g++ gcc-mingw-w64-x86_64 qemu-system-x86 ovmf
```
Windows系统中，可以下载我准备好的[编译工具](https://gitee.com/LinChenjun2008/build_tools)(不包含全部)
# 编译
1. 获取[源代码](https://github.com/linchenjun2008/pencil-kernel),
并创建`esp`文件夹以获取编译结果.
2. 进入`esp`目录,创建`EFI/Boot/`和`Kernel`文件夹.
```sh
cd ./esp
mkdir EFI
mkdir Kernel
cd ./EFI
mkdir Boot
```
3. 编译`build/kallsyms.cpp`,按编译应用程序的方式编译即可.
4. 打开 `pencil-kernel/build/config.txt` 并配置以下内容:
* `DISK`:上文创建的`esp`目录.
* `PHYSICAL_DISK`: 用于安装Pencil-Kernel的物理磁盘(可选).

示例:
```makefile
DISK = C:\pencil-kernel\esp
PHYSICAL_DISK =
```
* `Mingw_CC`: Mingw GCC 的路径.
* `CC`: x86_64-elf-tools GCC 的路径.
* `LD`: x86_64-elf-tools 连接器的路径.
* `GDB`: gdb 调试器(可选).
* `NM`
* `OBJCOPY`
* `OBJDUMP`
* `RM`:(可选).
* `KALLSYMS`: 步骤3中编译的kallsyms路径.
* `QEMU`:qemu模拟器
* `UEFI_BIOS`:Ovmf文件

示例:
1. Windows
```makefile
Mingw_CC  = Mingw64/bin/gcc
CC        = x86_64-elf-tools/bin/x86_64-elf-gcc.exe
LD        = x86_64-elf-tools/x86_64-elf/bin/ld.exe
GDB       = MinGW64/bin/gdb.exe
NM        = x86_64-elf-tools/x86_64-elf/bin/nm.exe
OBJCOPY   = x86_64-elf-tools/x86_64-elf/bin/objcopy.exe
OBJDUMP   = x86_64-elf-tools/x86_64-elf/bin/objdump.exe
RM        = rm.exe
KALLSYMS  = build/kallsyms.exe
QEMU      = qemu/qemu-system-x86_64.exe
UEFI_BIOS = vm/ovmf.bin
```
2. Linux
```makefile
Mingw_CC  = x86_64-w64-mingw32-gcc
CC        = gcc
LD        = ld
GDB       = gdb
NM        = nm
OBJCOPY   = objcopy
OBJDUMP   = objdump
RM        = rm
KALLSYMS  = build/kallsyms
QEMU      = qemu-system-x86_64.exe
UEFI_BIOS = ovmf.fd
```
5. 进入`pencil-kernel/build/`目录并执行`make bootoader`和`make kernel`.

示例:
```
cd C:/pencil-kernel/build
make bootloader
make kernel
```
如果一切顺利,你可以在`esp`文件夹中找到编译结果.
# 安装
1. 进入`esp/Kernel`目录
2. 创建`resource`文件夹.
3. 将点阵字体文件(可选)和truetype(.ttf)格式的字体文件放在`resource`目录中.修改`src/bootloader/boot.h`,然后进入`pencil-kernel/build/`目录并执行`make bootoader`重新编译bootloader
```c
struct Files Files[] =
{
    {L"Kernel\\kernel.sys",0x100000,AllocateAddress,0x80000001},
    // 改为点阵字体文件路径
    {L"Kernel\\typeface.sys",0x600000,AllocateAddress,0x80000002},
    // 改为truetype字体路径
    {L"Kernel\\resource\\typeface.ttf",0x800000,AllocateAddress,0x80000003}
};

```
4. 将`esp`复制到用于安装Pencil-Kernel的磁盘中.
# 在qemu中运行
1. 按照[安装](#安装)过程的`1-3`步执行
2. 进入`pencil-kernel/build/`目录并执行`make run`.

# 参考资料
1. 郑刚.操作系统真相还原\[M\].北京:人民邮电出版社,2016.
2. Hidemi Kawai.30天自制操作系统\[M\].周自恒,李黎明,曾祥江,张文旭译.北京:人民邮电出版社,2012.
3. 于渊.Orange'S: 一个操作系统的实现\[M\].北京:电子工业出版社,2009.
4. 大神 祐真.フルスクラッチで作る!UEFIベアメタルプログラミング\[M/OL\].https://kagurazakakotori.github.io/ubmp-cn/.
