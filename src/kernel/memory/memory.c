#include <memory.h>
#include <bitmap.h>
#include <debug.h>
#include <Efi.h>
#include <interrupt/interrupt.h>
#include <device/serial.h>
#include <std/string.h>

#define PAGE_BITMAP_BYTES_LEN 2048

typedef enum
{
    FREE_MEMORY = 1,
    RESERVED_MEMORY,
    ACPI_MEMORY,
    ACPI_MEMORY_NVS,
    UNUSEABLE_MEMORY,
    MAX_MEMORY_TYPE,
} memory_type_t;

// 不同大小和类型的内存组
typedef struct
{
    size_t   block_size;
    uint64_t total_free;
    list_t   free_block_list;
} memory_group_t;

PRIVATE memory_group_t memory_groups[NUMBER_OF_MEMORY_BLOCK_TYPES];

PRIVATE bitmap_t page_bitmap;
PRIVATE uint8_t  page_bitmap_map[PAGE_BITMAP_BYTES_LEN];


typedef list_node_t memory_block_t;

typedef struct
{
    memory_group_t *group;
    uint64_t        number_of_blocks;
    size_t          cnt;
    bool            large;
} zone_t;

size_t page_index_high(uintptr_t page_addr)
{
    return DIV_ROUND_UP(page_addr,PG_SIZE);
}

size_t page_index_low(uintptr_t page_addr)
{
    return page_addr / PG_SIZE;
}

PRIVATE memory_type_t type_uefi2os(EFI_MEMORY_TYPE efi_type)
{
    memory_type_t type;
    switch(efi_type)
    {
        case EfiConventionalMemory:
        case EfiBootServicesCode:
        case EfiBootServicesData:
        case EfiLoaderCode:
        case EfiLoaderData:
            type = FREE_MEMORY;
            break;
        case EfiRuntimeServicesCode:
        case EfiRuntimeServicesData:
        case EfiMemoryMappedIO:
        case EfiMemoryMappedIOPortSpace:
        case EfiPalCode:
        case EfiReservedMemoryType:
            type = RESERVED_MEMORY;
            break;
        case EfiACPIReclaimMemory:
            type = ACPI_MEMORY;
            break;
        case EfiACPIMemoryNVS:
            type = ACPI_MEMORY_NVS;
            break;
        case EfiUnusableMemory:
        case EfiMaxMemoryType:
            type = UNUSEABLE_MEMORY;
            break;
        default:
            type = MAX_MEMORY_TYPE;
            break;
    }
    return type;
}


PRIVATE void init_memory_group()
{
    int group_block_size = MIN_ALLOCATE_MEMORY_SIZE;
    int i;
    for (i = 0;i < NUMBER_OF_MEMORY_BLOCK_TYPES;i++)
    {
        memory_groups[i].block_size            = group_block_size;
        memory_groups[i].total_free            = 0;
        list_init(&memory_groups[i].free_block_list);
        group_block_size *= 2;
    }
    return;
}

PRIVATE void make_page_table(uintptr_t max_address)
{
    uint8_t* paddr;
    for ( paddr = (void*)0x10000000UL;
          paddr <= (uint8_t*)max_address;
          paddr += PG_SIZE )
    {
        page_map((uint64_t*)KERNEL_PAGE_DIR_TABLE_POS,paddr,paddr);
    }
    return;
}

void init_memory()
{
    page_bitmap.map = page_bitmap_map;
    page_bitmap.btmp_bytes_len = PAGE_BITMAP_BYTES_LEN;
    bitmap_init(&page_bitmap);
    int number_of_memory_desct;
    number_of_memory_desct = g_boot_info.memory_map.map_size
                           / g_boot_info.memory_map.descriptor_size;
    uintptr_t total_free = 0;
    uintptr_t max_address = 0;
    int i;
    for (i = 0;i < number_of_memory_desct;i++)
    {
        EFI_MEMORY_DESCRIPTOR* efi_memory_desc =
                          (EFI_MEMORY_DESCRIPTOR*)g_boot_info.memory_map.buffer;
        uintptr_t start = efi_memory_desc[i].PhysicalStart;
        uintptr_t end   = start + (efi_memory_desc[i].NumberOfPages << 12);
        max_address     = efi_memory_desc[i].PhysicalStart
                          + (efi_memory_desc[i].NumberOfPages << 12);
        if (type_uefi2os(efi_memory_desc[i].Type) != FREE_MEMORY)
        {
            start = page_index_low(start);
            end   = page_index_high(end);
            uint64_t j;
            for(j = start;j < end;j++)
            {
                bitmap_set(&page_bitmap,j,1);
            }
        }
        else
        {
            start = page_index_high(start);
            end   = page_index_low(end);
            if(end >= start)
            {
                total_free += (end - start) * PG_SIZE;
            }
        }
    }
    if (max_address / PG_SIZE < PAGE_BITMAP_BYTES_LEN)
    {
        page_bitmap.btmp_bytes_len = max_address / PG_SIZE;
    }
    // 剔除被占用的内存(0 - 16M)
    for (i = 0;i < 8;i++)
    {
        bitmap_set(&page_bitmap,i,1);
    }
    init_memory_group();
    if (max_address > 0xffffffff)
    {
        make_page_table(max_address);
    }
    return;
}

PUBLIC void* alloc_physical_page(uint64_t number_of_pages)
{
    if (number_of_pages == 0)
    {
        return NULL;
    }
    intr_status_t intr_status = intr_disable();
    signed int index = bitmap_alloc(&page_bitmap,number_of_pages);
    uintptr_t paddr = 0;
    if (index != -1)
    {
        uint64_t i;
        for (i = index;i < index + number_of_pages;i++)
        {
            bitmap_set(&page_bitmap,i,1);
        }
        paddr = (0UL + (uintptr_t)index * PG_SIZE);
        memset(KADDR_P2V(paddr),0,number_of_pages * PG_SIZE);
    }
    intr_set_status(intr_status);
    return (void*)paddr;
}

PUBLIC void free_physical_page(void* addr,uint64_t number_of_pages)
{
    if (number_of_pages == 0)
    {
        LOG_ERROR("free page count(%d) error\n",number_of_pages);
        return;
    }
    if (addr == NULL || ((((uintptr_t)addr) & 0x1fffff) != 0))
    {
        LOG_ERROR("%p not a page addr\n",addr);
        return;
    }
    intr_status_t intr_status = intr_disable();
    uintptr_t i;
    for (i = (uintptr_t)addr / PG_SIZE;
         i < (uintptr_t)addr / PG_SIZE + number_of_pages;i++)
    {
        bitmap_set(&page_bitmap,i,0);
    }
    intr_set_status(intr_status);
    return;
}

PRIVATE memory_block_t* znoe2block(zone_t* z,int idx)
{
    uintptr_t addr = (uintptr_t)z + sizeof(*z) + (PG_SIZE - sizeof(*z))
                    % z->group->block_size;
    return ((memory_block_t*)(addr + (idx * (z->group->block_size))));
}

PRIVATE zone_t* block2zone(memory_block_t* b)
{
    return ((zone_t*)((uintptr_t)b & 0xffffffffffe00000));
}

PUBLIC void* pmalloc(size_t size)
{
    int i;
    zone_t *z;
    memory_block_t *b;
    if (size > MAX_ALLOCATE_MEMORY_SIZE)
    {
        size_t number_of_pages;
        number_of_pages = DIV_ROUND_UP(size + sizeof(*z),PG_SIZE);
        z = KADDR_P2V(alloc_physical_page(number_of_pages));
        if (z == NULL)
        {
            LOG_ERROR("Out of memory\n");
            return NULL;
        }
        z->group = NULL;
        z->cnt = number_of_pages;
        z->large = TRUE;
        return (void*)KADDR_V2P(z + 1);
    }
    for (i = 0;i < NUMBER_OF_MEMORY_BLOCK_TYPES;i++)
    {
        if (size <= memory_groups[i].block_size)
        {
            break;
        }
    }
    if (list_empty(&memory_groups[i].free_block_list))
    {
        z = KADDR_P2V(alloc_physical_page(1));
        if (z == NULL)
        {
            LOG_ERROR("Out of memory\n");
            return NULL;
        }
        memset(z,0,PG_SIZE);
        z->group            = &memory_groups[i];
        z->large            = FALSE;
        z->number_of_blocks = (PG_SIZE - sizeof(*z)
                                - (PG_SIZE - sizeof(*z)) % z->group->block_size)
                                / z->group->block_size;
        z->cnt              = z->number_of_blocks;
        memory_groups[i].total_free += z->cnt;
        size_t block_index;
        intr_status_t intr_status = intr_disable();
        for (block_index = 0;block_index < z->cnt;block_index++)
        {
            b = znoe2block(z,block_index);
            list_append(&z->group->free_block_list,b);
        }
        intr_set_status(intr_status);
    }
    b = list_pop(&memory_groups[i].free_block_list);
    memset(b,0,memory_groups[i].block_size);
    z = block2zone(b);
    z->cnt--;
    memory_groups[i].total_free--;
    return (void*)KADDR_V2P(b);
}


PUBLIC void pfree(void* addr)
{
    if (addr == NULL)
    {
        return;
    }
    zone_t* z;
    memory_block_t* b;
    b = KADDR_P2V(addr);
    z = block2zone(b);
    if (z->group == NULL && z->large == TRUE)
    {
        free_physical_page(KADDR_V2P(z),z->cnt);
        return;
    }
    list_append(&z->group->free_block_list,b);
    z->cnt++;
    if (z->cnt == z->number_of_blocks && z->group->total_free >= z->number_of_blocks * 3 / 2)
    {
        size_t idx;
        for (idx = 0;idx < z->number_of_blocks;idx++)
        {
            b = znoe2block(z,idx);
            list_remove(b);
        }
        free_physical_page(KADDR_V2P(z),1);
    }
    return;
}

PUBLIC uint64_t* pml4t_entry(void* pml4t,void* vaddr)
{
    return (uint64_t*)pml4t + ADDR_PML4T_INDEX(vaddr);
}

PUBLIC uint64_t* pdpt_entry(void* pml4t,void* vaddr)
{
    return (uint64_t*)(*(uint64_t*)KADDR_P2V(pml4t_entry(pml4t,vaddr)) & ~0xfff)
            + ADDR_PDPT_INDEX(vaddr);
}

PUBLIC uint64_t* pdt_entry(void* pml4t,void* vaddr)
{
    return (uint64_t*)(*(uint64_t*)KADDR_P2V(pdpt_entry(pml4t,vaddr)) & ~0xfff)
            + ADDR_PDT_INDEX(vaddr);
}

PUBLIC void* to_physical_address(void* pml4t,void* vaddr)
{
    return (void*)
        ( (*(uint64_t*)KADDR_P2V(pdt_entry(pml4t,vaddr)) & ~0xfff)
        + ADDR_OFFSET(vaddr));
}

/**
 * @brief 将paddr映射到虚拟地址vaddr处
 * @param pml4t 页目录表地址
 * @param paddr 物理地址
 * @param vaddr 虚拟地址
*/
PUBLIC void page_map(uint64_t* pml4t,void* paddr,void* vaddr)
{
    paddr = (void*)((uintptr_t)paddr & ~(PG_SIZE - 1));
    vaddr = (void*)((uintptr_t)vaddr & ~(PG_SIZE - 1));
    uint64_t *v_pml4t,*v_pml4e;
    uint64_t *v_pdpt,*pdpt,*v_pdpte,*pdpte;
    uint64_t *v_pdt,*pdt,*v_pde,*pde;
    v_pml4t = KADDR_P2V(pml4t);
    v_pml4e = v_pml4t + ADDR_PML4T_INDEX(vaddr);
    if (!(*v_pml4e & PG_P))
    {
        pdpt = pmalloc(PT_SIZE);
        v_pdpt = KADDR_P2V(pdpt);
        memset(v_pdpt,0,PT_SIZE);
        *v_pml4e = (uint64_t)pdpt | PG_US_U | PG_RW_W | PG_P;
    }
    pdpt = (uint64_t*)(*v_pml4e & (~0xfff));
    pdpte = pdpt + ADDR_PDPT_INDEX(vaddr);
    v_pdpte = KADDR_P2V(pdpte);
    if (!(*v_pdpte & PG_P))
    {
        pdt = pmalloc(PT_SIZE);
        v_pdt = KADDR_P2V(pdt);
        memset(v_pdt,0,PT_SIZE);
        *v_pdpte = (uint64_t)pdt | PG_US_U | PG_RW_W | PG_P;
    }
    pdt = (uint64_t*)(*v_pdpte & (~0xfff));
    pde = pdt + ADDR_PDT_INDEX(vaddr);
    v_pde = KADDR_P2V(pde);
    *v_pde = (uint64_t)paddr | PG_US_U | PG_RW_W | PG_P | PG_SIZE_2M;
}

/**
 * @brief 取消内存的映射
 * @param pml4t 页目录地址
 * @param vaddr 要取消映射的虚拟地址
*/
void page_unmap(uint64_t* pml4t,void* vaddr)
{
    vaddr = (void*)((uintptr_t)vaddr & ~(PG_SIZE - 1));
    uint64_t *v_pml4t,*v_pml4e;
    uint64_t *pdpt,*v_pdpte,*pdpte;
    uint64_t *pdt,*v_pde,*pde;
    v_pml4t = KADDR_P2V(pml4t);
    v_pml4e = v_pml4t + ADDR_PML4T_INDEX(vaddr);
    if (!(*v_pml4e & PG_P))
    {
        return;
    }
    pdpt = (uint64_t*)(*v_pml4e & (~0xfff));
    pdpte = pdpt + ADDR_PDPT_INDEX(vaddr);
    v_pdpte = KADDR_P2V(pdpte);
    if (!(*v_pdpte & PG_P))
    {
        return;
    }
    pdt = (uint64_t*)(*v_pdpte & (~0xfff));
    pde = pdt + ADDR_PDT_INDEX(vaddr);
    v_pde = KADDR_P2V(pde);
    *v_pde &= ~PG_P;
}