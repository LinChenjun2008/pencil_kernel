#include <service.h>

pid_t service_pid[SRV_CNT + 1];

void mm_main();
void view_main();
void tick_main();

PUBLIC void start_service()
{
    service_pid[                    0] = 0;
    service_pid[SRV_MM   - 0x80000000] =
                                    thread_start(  "MM",63,  mm_main,NULL)->pid;
    service_pid[SRV_VIEW - 0x80000000] =
                                    thread_start("VIEW",63,view_main,NULL)->pid;
    service_pid[SRV_TICK - 0x80000000] =
                                    thread_start("TICK",63,tick_main,NULL)->pid;
}