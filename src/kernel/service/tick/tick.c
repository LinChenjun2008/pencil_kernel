#include <service.h>
#include <std/stdint.h>
#include <debug.h>

uint64_t global_ticks;

PUBLIC void tick_main()
{
    message_t msg;
    while(1)
    {
        send_recv(NR_RECEIVE,ANY,&msg);
        switch(msg.type)
        {
            case TICK_GET_TICKS:
                msg.msg3.m3l1 = global_ticks;
                LOG_INFO("ticks = %d\n",global_ticks);
                send_recv(NR_SEND,msg.source,&msg);
                break;
            default:
                LOG_ERROR("unknow msg.type: %d\n",msg.type);
                break;
        }
    }
}