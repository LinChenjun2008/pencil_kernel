#include <common.h>
#include <device/cpu.h>
#include <debug.h>
#include <init.h>
#include <interrupt/interrupt.h>
#include <memory.h>
#include <thread/thread.h>
#include <time.h>
#include <process.h>
#include <graphic.h>
#include <std/stdio.h>
#include <service.h>
#include <syscall.h>
#include <device/serial.h>

#include <device/acpi.h>
#include <device/pic.h>

boot_info_t g_boot_info;
void k_prog(void* arg);

PUBLIC uint64_t kernel_main()
{
    intr_disable();
    init_serial(COM1_PORT);
    // 检查运行环境
    g_boot_info = *((boot_info_t*)0xffff800000310000);
    if(g_boot_info.magic != 0x5a42cb1613d4a62f)
    {
        LOG_ERROR("boot info check failed!\n");
        while (1);
    };
    init_all();
    fpu_init();
    start_service();
    intr_enable();
    // process_execute(k_prog,"Kernel prog");

    while (1)
    {
        thread_block(TASK_BLOCKED);
    };
    return 0;
}

////////////////////////////////////////////////////////////////////////////
//  下面是测试进程
////////////////////////////////////////////////////////////////////////////

// #include <std/string.h>

// void k_prog(void* arg __attribute((unused)))
// {
//     PRIVATE pixel_t col =
//     {
//         .red = 128,
//         .green = 128,
//         .blue = 128,
//     };
//     message_t msg;
//     msg.type = MM_GET_A_PAGE;
//     __asm__ __volatile__("movq %%rsp,%0":"=r"(msg.msg3.m3l1));
//     send_recv(NR_BOTH,SRV_MM,&msg);
//     while (1)
//     {
//         col.red+=10;
//         view_fill(&(g_boot_info.graph_info),col,20,0,30,10);

//         if (col.red > 200)
//         {
//             message_t msg;
//             msg.type = MM_EXIT;
//             send_recv(NR_BOTH,SRV_MM,&msg);
//         }
//     };
// }