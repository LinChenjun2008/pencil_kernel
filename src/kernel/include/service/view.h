#ifndef __VIEW_H__
#define __VIEW_H__

#include <kernel/global.h>
#include <graphic.h>
#include <thread/thread.h>

typedef struct _viewctl_t viewctl_t;
typedef struct
{
    viewctl_t   *ctl;
    pid_t        holder;
    graph_info_t info;
    position_t   position;
    int32_t      height;
    uint8_t      flag;
} view_t;


struct _viewctl_t
{
    view_t      **views;
    view_t       *view0;
    uint32_t      number_of_views;
    int           top;
    graph_info_t  info;
    uintptr_t    *map;
};

#endif