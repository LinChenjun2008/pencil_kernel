#ifndef __SRVTEM_H__
#define __SRVTEM_H__

#include <interrupt/interrupt.h>
#include <syscall.h>
#include <thread/thread.h>

#define SRV_CNT 3

#define SRV_MM   0x80000001
#define SRV_VIEW 0x80000002
#define SRV_TICK 0x80000003

extern pid_t service_pid[SRV_CNT + 1];

#define MM_FORK           0x80000001
#define MM_WAIT           0x80000002
#define MM_EXIT           0x80000003
#define MM_GET_A_PAGE     0x80000004 /* 获取一个页 */
#define MM_FREE_A_PAGE    0x80000005 /* 释放一个页 */
#define MM_READ_PROC_ADDR 0x80000006 /* 读取某个进程的内存空间 */

#define TICK_GET_TICKS    0x80000001

PUBLIC void start_service();

#endif