#include <kernel/global.h>
#include <interrupt/interrupt.h>
#include <device/serial.h>
#include <std/stdio.h>
#include <std/stdarg.h>

#include <thread/thread.h>

PRIVATE char s[512];

void klog(int level,const char* func,int line,const char* format,...)
{
    switch (level)
    {
    case 1:
        pr_log(COM1_PORT,"\033[32m[ info  ]\033[0m ");
        break;
    case 2:
        pr_log(COM1_PORT,"\033[33m[ debug ]\033[0m ");
        break;
    case 3:
        pr_log(COM1_PORT,"\033[31m[ error ]\033[0m ");
        break;
    default:
        break;
    }
    intr_status_t intr_status = intr_disable();
    sprintf(s,"( %s ) %s: %d: ",running_thread()->name,func,line);
    pr_log(COM1_PORT,s);

    va_list ap;
    va_start(ap,format);
    vsprintf(s,format,ap);
    va_end(ap);

    pr_log(COM1_PORT,s);
    intr_set_status(intr_status);
}