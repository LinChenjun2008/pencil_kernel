#include <kernel/global.h>
#include <interrupt/interrupt.h>
#include <thread/thread.h>
#include <device/serial.h>
#include <std/stdio.h>

PRIVATE char str[512];
void panic_spin(const char* file,const char* base_file,int line,const char* func,
                const char* condition)
{
    intr_disable();
    sprintf(str,"--- Kernel Panic --- \n" \
                " File: %s \n" \
                " Base File: %s \n" \
                " In function: %s\n" \
                " Line: %d\n" \
                " Condition: %s\n" \
                " thread: %s\n",
                file,base_file,func,line,condition,running_thread()->name
            );
    pr_log(COM1_PORT,str);
    wordsize_t rbp;
    char* name;
    __asm__ __volatile__("movq %%rbp,%0":"=r"(rbp)::);
    int i;
    for (i = 0;i < 8;i++)
    {
        if (address_available(*((wordsize_t*)rbp + 1)))
        {
            name = address2symbol(*((wordsize_t*)rbp + 1));
            if (name)
            {
                pr_log(COM1_PORT," <+--- ");
                pr_log(COM1_PORT,name);
            }
        }
        else
        {
            break;
        }
        rbp = *((wordsize_t*)rbp);
    }
    while (1);
}